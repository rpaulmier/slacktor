FROM python:3.7-alpine

COPY . /app

WORKDIR /app

RUN apk add --no-cache openssh-client && \
    pip install pipenv && \
    pipenv install --system --ignore-pipfile --deploy && \
    adduser -D -u 1000 -g slacktor slacktor && \
    install -d -m 0755 -o slacktor -g slacktor /home/slacktor/.ssh 

USER slacktor:slacktor

CMD ["/app/slacktor.py"]
