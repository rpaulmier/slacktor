#!/usr/bin/env python3

import os
import re
import subprocess
import sys
import threading
import time

import click
import slack
import slack.errors
import yaml


SLACKTOR_API_TOKEN = os.environ.get('SLACKTOR_API_TOKEN', '<invalid token>')
SLACKTOR_CONFIG = os.environ.get('SLACKTOR_CONFIG', os.path.join(os.path.dirname(__file__), 'slacktor.yaml'))
PIPELINE_PASSED_REGEX = re.compile(r'Pipeline #([a-zA-Z0-9]+) has passed in [0-9]+:[0-9]+')

try:
    with open(SLACKTOR_CONFIG, 'r') as _f:
        CONFIG = yaml.load(_f, Loader=yaml.FullLoader)
except (OSError, yaml.error.YAMLError) as e:
    click.secho(str(e), fg='red')
    sys.exit(255)


def detect_pipeline_ok(data):
    subtype = data.get("subtype")
    if subtype != 'bot_message':
        return None, None

    attachments = data.get('attachments', [])
    if len(attachments) != 1:
        click.secho(f"1 attachment is expected", fg='red')
        return None, None

    title = attachments[0].get('title')
    match = PIPELINE_PASSED_REGEX.match(title)
    if not match:
        return None, None
    pipeline = match.group(1)

    fields = attachments[0].get('fields', [])
    projects = [field.get('value') for field in fields if field.get('title') == 'Branch']
    if len(projects) != 1:
        click.secho(f"1 field with title==Branch is expected", fg='red')
        return pipeline, None

    project = projects[0]
    for name in CONFIG.get('projects', {}).keys():
        p = CONFIG['projects'][name]
        full_url = f"{p['url']}/commits/{p['branch']}"
        if re.search(full_url, project):
            return pipeline, name

    return pipeline, None


def deploy_project(project, channel, pipeline, web_client):
    def run_on_thread():
        p = subprocess.Popen(
            CONFIG['projects'][project]['cmd'], shell=True,
            stderr=subprocess.PIPE, stdout=subprocess.PIPE
        )
        try:
            timeout = 600
            returncode = p.wait(timeout=timeout)  # blocking
            stdout, stderr = p.communicate(timeout=timeout)
        except subprocess.TimeoutExpired:
            p.terminate()
            time.sleep(1)
            p.kill()
            returncode = p.wait()
            stdout, stderr = p.communicate()
        stdout = stdout.decode()
        stderr = stderr.decode()

        if returncode == 0:
            message = f"*{project}* was successfully deployed."
            click.secho(message, fg='green')
        else:
            message = f"*{project}* deployment failed ({returncode})."
            click.secho(message, fg='red')

        attachments = [{
            "title": f"Deployment status for pipeline #{pipeline}",
            "text": message,
            "color": "good" if returncode == 0 else "danger"
        }]
        if len(stdout) > 0:
            attachments.append({
                "mrkdwn_in": ["text"],
                "title": "Standard output",
                "text": f"```{stdout}```",
                "color": "good" if returncode == 0 else "danger"
            })
        if len(stderr) > 0:
            attachments.append({
                "mrkdwn_in": ["text"],
                "title": "Standard error",
                "text": f"```{stderr}```",
                "color": "warning" if returncode == 0 else "danger"
            })

        web_client.chat_postMessage(
            channel=channel,
            text='',
            attachments=attachments,
        )

    thread = threading.Thread(target=run_on_thread)
    thread.start()  # non blocking
    return thread


@slack.RTMClient.run_on(event="message")
def handle_message(**payload):
    data = payload["data"]
    web_client: slack.WebClient = payload["web_client"]
    channel = data.get('channel')

    click.secho(str(data), fg='magenta')

    pipeline, project = detect_pipeline_ok(data)
    if project:
        message = f"Pipeline #{pipeline} for {project} in branch {CONFIG['projects'][project]['branch']} was OK, " \
            "let's deploy it"
        click.secho(message, fg='green')

        web_client.chat_postMessage(
            channel=channel,
            text=message,
        )

        deploy_project(project, channel, pipeline, web_client)


@click.command()
def slacktor():
    click.secho('Slacktor starting up !', fg='green')

    try:
        web_client = slack.WebClient(token=SLACKTOR_API_TOKEN)
        web_client.api_test()  # a simple call to detect potential auth errors before going to RTM.
    except slack.errors.SlackClientError as e:
        click.secho(str(e), fg='red')
        sys.exit(255)

    rtm_client = slack.RTMClient(token=SLACKTOR_API_TOKEN)
    rtm_client.start()


if __name__ == '__main__':
    slacktor()
