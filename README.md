# WTF is slacktor ?

Slacktor is a slack - reactor bot. 

Its purpose is to react to some particular messages sent to him, via a channel or DM.

Currently, the only message type that is recognized is Gitlab notification when a pipeline has successfully passed.

# Install

Go to https://api.slack.com/apps
Create New App, name Slacktor
Add Features:
 - "Incoming Webhooks", to the channel you want (eg: #fr-dev-cicd)
 - "Bot Users", add a bot, named "Slacktor" with default username "slacktor"
 - add the following permissions:
 - send messages as Slacktor (chat:write:bot)
 
 Then install app, and gather Bot User OAuth Access Token
 
 If channel is private, don't forget to invite the bot to the channel !
 
# Configuration

```yaml
---
projects:
    project1:  # the project name
        url: https://gitlab.com/some/path/project1  # the project path in gitlab
        branch: master  # the branch we wanna trigger deployment for
        cmd: >  # the command we wannt trigger to actually deploy
            ssh someone@somewhere
            'some remote shell
            multi-line command'
    project2:
      [...]
```

# Run with docker-compose

```yaml
---
version: '2'
services:
  slacktor:
    image: 'slacktor:latest'
    restart: always
    environment:
      SLACKTOR_API_TOKEN: "<slack bot OAuth Access Token>"
      SLACKTOR_CONFIG: "/home/slacktor/slacktor.yaml"
    volumes:
      - '/some/where/slacktor.yaml:/home/slacktor/slacktor.yaml:ro'
      - '/some/where/id_rsa:/home/slacktor/.ssh/id_rsa:ro'
      - '/some/where/id_rsa.pub:/home/slacktor/.ssh/id_rsa.pub:ro'
```

